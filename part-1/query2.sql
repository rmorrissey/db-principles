USE RMorrissey

-- Write a query to display a list of books written by authors who live in New Jersey
-- ID, Title, Author First & Last Name, City, State
SELECT b.BookId,
	b.Title,
	a.FirstName,
	a.LastName,
	aa.City,
	aa.STATE
FROM dbo.Books b
JOIN Authors a ON a.AuthorId = b.Author
JOIN Address aa ON a.AddressId = aa.AddressId
WHERE aa.STATE = 'NJ'
GO
