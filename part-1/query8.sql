USE RMorrissey

-- Write a query to show the list of customers and the amount they spent till date.
-- ID, Customer First & Last Name, amount spent
SELECT c.CustomerId,
	c.LastName,
	c.FirstName,
	ISNULL(SUM(l.Price * l.Quantity), 0) AS TotalSpent
FROM dbo.Customers c
LEFT OUTER JOIN Orders o ON o.Customer = c.CustomerId
LEFT OUTER JOIN OrderLines l ON o.OrderId = l.[Order]
GROUP BY c.CustomerId,
	c.LastName,
	c.FirstName
GO
