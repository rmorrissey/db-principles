USE RMorrissey

-- Write a query to show each line item of each order
-- Order ID, Order Date, Customer First & Last Name, Book Title, Quantity, Cost.
SELECT o.orderId,
	o.DateCreated,
	c.FirstName,
	c.LastName,
	b.Title,
	l.Quantity,
	l.Price
FROM dbo.Orders o
JOIN Customers c ON o.Customer = c.CustomerId
JOIN OrderLines l ON o.OrderId = l.[Order]
JOIN Books b ON l.Book = b.BookId
GO
