USE RMorrissey

-- Write a query to show each order total
-- Order ID, Order Date, Customer First & Last Name, Total Amount, Order Status
SELECT o.orderId,
	MAX(o.DateCreated),
	MAX(c.FirstName),
	MAX(c.LastName),
	MAX(b.Title),
	SUM(l.Quantity * l.Price) AS Total
FROM dbo.Orders o
JOIN Customers c ON o.Customer = c.CustomerId
JOIN OrderLines l ON o.OrderId = l.[Order]
JOIN Books b ON l.Book = b.BookId
GROUP BY o.OrderId
GO
