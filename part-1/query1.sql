USE RMorrissey

-- Write a query that displays all active (non-Archived) books in the system.
-- ID, Title, Author First & Last Name
SELECT b.BookId,
	b.Title,
	a.FirstName,
	a.LastName
FROM dbo.Books b
JOIN Authors A ON a.AuthorId = b.Author
WHERE b.Archived = 0
GO
