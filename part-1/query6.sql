USE RMorrissey

-- Write a query to display all the authors that have at least one active book in the system. Show the count of active books per author.
-- ID, Author First & Last Name, Count of active books
SELECT a.AuthorId,
	a.LastName,
	a.FirstName,
	COUNT(b.BookId) AS ActiveCount
FROM dbo.Authors a
JOIN Books b ON b.Author = a.AuthorId
WHERE b.Archived = 0
GROUP BY a.AuthorId,
	a.LastName,
	a.FirstName
GO
