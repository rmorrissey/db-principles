USE RMorrissey

-- Write a query to create a list of best-selling books.
-- ID, Title, Count of books sold.
SELECT TOP (5) b.BookId,
	b.Title,
	SUM(l.Quantity) AS QuantitySold
FROM dbo.Books b
JOIN OrderLines l ON l.Book = b.BookId
WHERE b.Archived = 0
GROUP BY b.BookId,
	b.Title
ORDER BY QuantitySold DESC
GO
