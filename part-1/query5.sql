USE RMorrissey

-- Write a query to display all the active books in the system with the Qty sold to date.
-- ID, Title, Qty Sold to Date
SELECT b.BookId,
	b.Title,
	SUM(l.Quantity) AS QuantitySold
FROM dbo.Books b
JOIN OrderLines l ON l.Book = b.BookId
WHERE b.Archived = 0
GROUP BY b.BookId,
	b.Title
GO
