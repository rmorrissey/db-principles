USE [RMorrissey]
GO
/****** Object:  Table [dbo].[Address]    Script Date: 11/12/2020 9:32:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Address](
	[AddressId] [int] IDENTITY(1,1) NOT NULL,
	[Archived] [bit] NOT NULL,
	[CreatedBy] [int] NULL,
	[DateCreated] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[DateModified] [datetime] NULL,
	[StreetLine1] [nvarchar](50) NOT NULL,
	[StreetLine2] [nvarchar](50) NULL,
	[City] [nvarchar](50) NOT NULL,
	[State] [nvarchar](2) NOT NULL,
	[ZipCode] [nvarchar](10) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[AddressId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Authors]    Script Date: 11/12/2020 9:32:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Authors](
	[AuthorId] [int] IDENTITY(1,1) NOT NULL,
	[Archived] [bit] NOT NULL,
	[CreatedBy] [int] NULL,
	[DateCreated] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[DateModified] [datetime] NULL,
	[FirstName] [nvarchar](50) NOT NULL,
	[LastName] [nvarchar](50) NOT NULL,
	[AddressId] [int] NULL,
	[PhoneNumber] [nvarchar](10) NULL,
	[FaxNumber] [nvarchar](10) NULL,
	[MobileNumber] [nvarchar](10) NULL,
PRIMARY KEY CLUSTERED 
(
	[AuthorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Books]    Script Date: 11/12/2020 9:32:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Books](
	[BookId] [int] IDENTITY(1,1) NOT NULL,
	[Archived] [bit] NOT NULL,
	[CreatedBy] [int] NULL,
	[DateCreated] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[DateModified] [datetime] NULL,
	[ISBN] [nvarchar](50) NOT NULL,
	[Title] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](max) NULL,
	[Author] [int] NOT NULL,
	[StockCount] [int] NOT NULL,
	[Cost] [money] NOT NULL,
	[Price] [money] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[BookId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Customers]    Script Date: 11/12/2020 9:32:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customers](
	[CustomerId] [int] IDENTITY(1,1) NOT NULL,
	[Archived] [bit] NOT NULL,
	[CreatedBy] [int] NULL,
	[DateCreated] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[DateModified] [datetime] NULL,
	[FirstName] [nvarchar](50) NOT NULL,
	[LastName] [nvarchar](50) NOT NULL,
	[AddressId] [int] NOT NULL,
	[PhoneNumber] [nvarchar](10) NULL,
	[FaxNumber] [nvarchar](10) NULL,
	[MobileNumber] [nvarchar](10) NULL,
PRIMARY KEY CLUSTERED 
(
	[CustomerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderLines]    Script Date: 11/12/2020 9:32:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderLines](
	[OrderLineId] [int] IDENTITY(1,1) NOT NULL,
	[Order] [int] NOT NULL,
	[Book] [int] NOT NULL,
	[Quantity] [int] NOT NULL,
	[Price] [money] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[OrderLineId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Orders]    Script Date: 11/12/2020 9:32:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Orders](
	[OrderId] [int] IDENTITY(1,1) NOT NULL,
	[Archived] [bit] NOT NULL,
	[CreatedBy] [int] NULL,
	[DateCreated] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[DateModified] [datetime] NULL,
	[Customer] [int] NOT NULL,
	[OrderStatus] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[OrderId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderStatus]    Script Date: 11/12/2020 9:32:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderStatus](
	[OrderStatusId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[OrderStatusId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 11/12/2020 9:32:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[Archived] [bit] NOT NULL,
	[CreatedBy] [int] NULL,
	[DateCreated] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[DateModified] [datetime] NULL,
	[FirstName] [nvarchar](50) NOT NULL,
	[LastName] [nvarchar](50) NOT NULL,
	[Email] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Address] ON 

INSERT [dbo].[Address] ([AddressId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [StreetLine1], [StreetLine2], [City], [State], [ZipCode]) VALUES (1, 0, 1, CAST(N'2020-11-11T20:27:23.953' AS DateTime), NULL, NULL, N'3950 Whitcomb St', NULL, N'Raymond', N'WA', N'98577')
INSERT [dbo].[Address] ([AddressId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [StreetLine1], [StreetLine2], [City], [State], [ZipCode]) VALUES (2, 0, 1, CAST(N'2020-11-11T20:27:23.953' AS DateTime), NULL, NULL, N'787 A Cox Rd', NULL, N'Elk Horn', N'KY', N'42733')
INSERT [dbo].[Address] ([AddressId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [StreetLine1], [StreetLine2], [City], [State], [ZipCode]) VALUES (3, 0, 1, CAST(N'2020-11-11T20:27:23.953' AS DateTime), NULL, NULL, N'1723 Woodcock Rd', NULL, N'Sequim', N'WA', N'98382')
INSERT [dbo].[Address] ([AddressId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [StreetLine1], [StreetLine2], [City], [State], [ZipCode]) VALUES (4, 0, 1, CAST(N'2020-11-11T20:27:23.953' AS DateTime), NULL, NULL, N'1057 W Century Dr', N'#216', N'Louisville', N'CO', N'80027')
INSERT [dbo].[Address] ([AddressId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [StreetLine1], [StreetLine2], [City], [State], [ZipCode]) VALUES (5, 0, 1, CAST(N'2020-11-11T20:27:23.953' AS DateTime), NULL, NULL, N'1711 W Jackson St', NULL, N'Covington', N'VA', N'24426')
INSERT [dbo].[Address] ([AddressId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [StreetLine1], [StreetLine2], [City], [State], [ZipCode]) VALUES (6, 0, 1, CAST(N'2020-11-11T20:27:23.953' AS DateTime), NULL, NULL, N'202 Us 69 Hwy N', NULL, N'Lone Oak', N'TX', N'75453')
INSERT [dbo].[Address] ([AddressId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [StreetLine1], [StreetLine2], [City], [State], [ZipCode]) VALUES (7, 0, 1, CAST(N'2020-11-11T20:27:23.953' AS DateTime), NULL, NULL, N'22 Dream Ln', NULL, N'Richmond', N'ME', N'04357')
INSERT [dbo].[Address] ([AddressId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [StreetLine1], [StreetLine2], [City], [State], [ZipCode]) VALUES (8, 0, 1, CAST(N'2020-11-11T20:27:23.953' AS DateTime), NULL, NULL, N'1567 Theatre St', NULL, N'Latrobe', N'PA', N'15650')
INSERT [dbo].[Address] ([AddressId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [StreetLine1], [StreetLine2], [City], [State], [ZipCode]) VALUES (9, 0, 1, CAST(N'2020-11-11T20:27:23.953' AS DateTime), NULL, NULL, N'1500 Gingell Rd', NULL, N'Johannesburg', N'MI', N'49751')
INSERT [dbo].[Address] ([AddressId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [StreetLine1], [StreetLine2], [City], [State], [ZipCode]) VALUES (10, 0, 1, CAST(N'2020-11-11T20:27:23.953' AS DateTime), NULL, NULL, N'7768 S Krameria Ct', NULL, N'Englewood', N'CO', N'80112')
INSERT [dbo].[Address] ([AddressId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [StreetLine1], [StreetLine2], [City], [State], [ZipCode]) VALUES (11, 0, 1, CAST(N'2020-11-11T20:27:23.953' AS DateTime), NULL, NULL, N'3951 Whitcomb St', NULL, N'Jersey City', N'NJ', N'98577')
INSERT [dbo].[Address] ([AddressId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [StreetLine1], [StreetLine2], [City], [State], [ZipCode]) VALUES (12, 0, 1, CAST(N'2020-11-12T09:19:30.017' AS DateTime), NULL, NULL, N'2930 167th St.', NULL, N'Flushing', N'NY', N'11358')
INSERT [dbo].[Address] ([AddressId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [StreetLine1], [StreetLine2], [City], [State], [ZipCode]) VALUES (14, 0, 1, CAST(N'2020-11-12T09:20:38.773' AS DateTime), NULL, NULL, N'3952  Frosty Lane', N'#2', N'Binghamton', N'NY', N'13901')
INSERT [dbo].[Address] ([AddressId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [StreetLine1], [StreetLine2], [City], [State], [ZipCode]) VALUES (15, 0, 1, CAST(N'2020-11-12T09:21:32.357' AS DateTime), NULL, NULL, N'4424  Lodgeville Road', NULL, N'Minneapolis', N'MN', N'55402')
INSERT [dbo].[Address] ([AddressId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [StreetLine1], [StreetLine2], [City], [State], [ZipCode]) VALUES (16, 0, 1, CAST(N'2020-11-12T09:22:11.807' AS DateTime), NULL, NULL, N'417  John Avenue', NULL, N'Grand Rapids', N'MI', N'49503')
INSERT [dbo].[Address] ([AddressId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [StreetLine1], [StreetLine2], [City], [State], [ZipCode]) VALUES (17, 0, 1, CAST(N'2020-11-12T09:22:52.040' AS DateTime), NULL, NULL, N'156  Quincy Street', NULL, N'Philadelphia', N'PA', N'19103')
INSERT [dbo].[Address] ([AddressId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [StreetLine1], [StreetLine2], [City], [State], [ZipCode]) VALUES (18, 0, 1, CAST(N'2020-11-12T09:23:33.017' AS DateTime), NULL, NULL, N'3295  Logan Lane', NULL, N'Denver', N'CO', N'80202')
INSERT [dbo].[Address] ([AddressId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [StreetLine1], [StreetLine2], [City], [State], [ZipCode]) VALUES (19, 0, 1, CAST(N'2020-11-12T09:24:38.853' AS DateTime), NULL, NULL, N'1725  Bubby Drive', NULL, N'Austin', N'TX', N'78704')
INSERT [dbo].[Address] ([AddressId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [StreetLine1], [StreetLine2], [City], [State], [ZipCode]) VALUES (20, 0, 1, CAST(N'2020-11-12T09:25:14.900' AS DateTime), NULL, NULL, N'5004  Pine Tree Lane', NULL, N'VISALIA', N'CA', N'93291')
INSERT [dbo].[Address] ([AddressId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [StreetLine1], [StreetLine2], [City], [State], [ZipCode]) VALUES (21, 0, 1, CAST(N'2020-11-12T09:27:11.027' AS DateTime), NULL, NULL, N'3562  Carolyns Circle', NULL, N'VIOLA', N'ID', N'83872')
SET IDENTITY_INSERT [dbo].[Address] OFF
GO
SET IDENTITY_INSERT [dbo].[Authors] ON 

INSERT [dbo].[Authors] ([AuthorId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [FirstName], [LastName], [AddressId], [PhoneNumber], [FaxNumber], [MobileNumber]) VALUES (1, 0, 1, CAST(N'2020-11-11T20:04:24.600' AS DateTime), NULL, NULL, N'George', N'Orwell', 12, N'1115551111', N'2225551111', N'3335551111')
INSERT [dbo].[Authors] ([AuthorId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [FirstName], [LastName], [AddressId], [PhoneNumber], [FaxNumber], [MobileNumber]) VALUES (2, 0, 1, CAST(N'2020-11-11T20:04:24.600' AS DateTime), NULL, NULL, N'Stephen', N'Hawking', 14, N'1115551112', N'2225551112', N'3335551112')
INSERT [dbo].[Authors] ([AuthorId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [FirstName], [LastName], [AddressId], [PhoneNumber], [FaxNumber], [MobileNumber]) VALUES (3, 0, 1, CAST(N'2020-11-11T20:04:24.600' AS DateTime), NULL, NULL, N'Dave', N'Eggers', 15, N'1115551113', N'2225551113', N'3335551113')
INSERT [dbo].[Authors] ([AuthorId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [FirstName], [LastName], [AddressId], [PhoneNumber], [FaxNumber], [MobileNumber]) VALUES (4, 0, 1, CAST(N'2020-11-11T20:04:24.600' AS DateTime), NULL, NULL, N'Ishmael', N'Beah', 16, N'1115551134', N'2225551114', N'3335551114')
INSERT [dbo].[Authors] ([AuthorId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [FirstName], [LastName], [AddressId], [PhoneNumber], [FaxNumber], [MobileNumber]) VALUES (5, 0, 1, CAST(N'2020-11-11T20:04:24.600' AS DateTime), NULL, NULL, N'Madeleine', N'L''Engle', 17, N'1115551115', N'2225551115', N'3335551115')
INSERT [dbo].[Authors] ([AuthorId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [FirstName], [LastName], [AddressId], [PhoneNumber], [FaxNumber], [MobileNumber]) VALUES (6, 0, 1, CAST(N'2020-11-11T20:04:24.600' AS DateTime), NULL, NULL, N'Lewis', N'Carroll', 18, N'1115551116', N'2225551116', N'3335551116')
INSERT [dbo].[Authors] ([AuthorId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [FirstName], [LastName], [AddressId], [PhoneNumber], [FaxNumber], [MobileNumber]) VALUES (7, 0, 1, CAST(N'2020-11-11T20:08:18.333' AS DateTime), NULL, NULL, N'Frank', N'Herbert', 11, N'1115551117', N'2225551117', N'3335551117')
INSERT [dbo].[Authors] ([AuthorId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [FirstName], [LastName], [AddressId], [PhoneNumber], [FaxNumber], [MobileNumber]) VALUES (8, 0, 1, CAST(N'2020-11-11T20:10:04.260' AS DateTime), NULL, NULL, N'Ray', N'Bradbury', 19, N'1115551118', N'2225551118', N'3335551118')
INSERT [dbo].[Authors] ([AuthorId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [FirstName], [LastName], [AddressId], [PhoneNumber], [FaxNumber], [MobileNumber]) VALUES (9, 0, 1, CAST(N'2020-11-11T20:10:31.217' AS DateTime), NULL, NULL, N'Gillian', N'Flynn', 20, N'1115551119', N'2225551119', N'3335551119')
INSERT [dbo].[Authors] ([AuthorId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [FirstName], [LastName], [AddressId], [PhoneNumber], [FaxNumber], [MobileNumber]) VALUES (10, 0, 1, CAST(N'2020-11-11T20:11:17.673' AS DateTime), NULL, NULL, N'Margaret', N'Brown', 21, N'1115551121', N'2225551121', N'3335551121')
SET IDENTITY_INSERT [dbo].[Authors] OFF
GO
SET IDENTITY_INSERT [dbo].[Books] ON 

INSERT [dbo].[Books] ([BookId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [ISBN], [Title], [Description], [Author], [StockCount], [Cost], [Price]) VALUES (1, 0, 1, CAST(N'2020-11-11T20:14:09.333' AS DateTime), NULL, NULL, N'9780451524935', N'1984', N'Winston Smith toes the Party line, rewriting history to satisfy the demands of the Ministry of Truth.', 1, 10, 2.5000, 6.7500)
INSERT [dbo].[Books] ([BookId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [ISBN], [Title], [Description], [Author], [StockCount], [Cost], [Price]) VALUES (2, 0, 1, CAST(N'2020-11-11T20:14:09.333' AS DateTime), NULL, NULL, N'9780553380163', N'A Brief History of Time', N'With exciting images and profound imagination, Stephen Hawking brings us closer to the ultimate secrets at the very heart of creation.', 2, 12, 3.0000, 7.7500)
INSERT [dbo].[Books] ([BookId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [ISBN], [Title], [Description], [Author], [StockCount], [Cost], [Price]) VALUES (3, 0, 1, CAST(N'2020-11-11T20:14:09.333' AS DateTime), NULL, NULL, N'0375725784', N'A Heartbreaking Work of Staggering Genius', N'The moving memoir of a college senior who, in the space of five weeks, loses both of his parents to cancer and inherits his eight-year-old brother.', 3, 4, 5.5000, 10.6900)
INSERT [dbo].[Books] ([BookId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [ISBN], [Title], [Description], [Author], [StockCount], [Cost], [Price]) VALUES (4, 0, 1, CAST(N'2020-11-11T20:14:09.333' AS DateTime), NULL, NULL, N'9780374531263', N'A Long Way Gone: Memoirs of a Boy Soldier', N'In the more than fifty conflicts going on worldwide, it is estimated that there are some 300,000 child soldiers. Ishmael Beah used to be one of them.', 4, 7, 2.2500, 7.9200)
INSERT [dbo].[Books] ([BookId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [ISBN], [Title], [Description], [Author], [StockCount], [Cost], [Price]) VALUES (5, 0, 1, CAST(N'2020-11-11T20:14:09.333' AS DateTime), NULL, NULL, N'0312367554', N'A Wrinkle in Time', N'The winner of the 1963 Newbery Medal', 5, 10, 3.5000, 5.3500)
INSERT [dbo].[Books] ([BookId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [ISBN], [Title], [Description], [Author], [StockCount], [Cost], [Price]) VALUES (6, 0, 1, CAST(N'2020-11-11T20:14:09.333' AS DateTime), NULL, NULL, N'9780553213454', N'Alice''s Adventures in Wonderland', N'In 1862 Charles Lutwidge Dodgson, a shy Oxford mathematician with a stammer, created a story about a little girl tumbling down a rabbit hole. Thus began the immortal adventures of Alice, perhaps the most popular heroine in English literature.', 6, 2, 2.7500, 5.9500)
INSERT [dbo].[Books] ([BookId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [ISBN], [Title], [Description], [Author], [StockCount], [Cost], [Price]) VALUES (8, 0, 1, CAST(N'2020-11-11T20:17:09.830' AS DateTime), NULL, NULL, N'0441013597', N'Dune', N'Set on the desert planet Arrakis, Dune is the story of the boy Paul Atreides, heir to a noble family tasked with ruling an inhospitable world where the only thing of value is the “spice” melange, a drug capable of extending life and enhancing consciousness. Coveted across the known universe, melange is a prize worth killing for....', 7, 13, 4.0000, 9.4900)
INSERT [dbo].[Books] ([BookId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [ISBN], [Title], [Description], [Author], [StockCount], [Cost], [Price]) VALUES (11, 0, 1, CAST(N'2020-11-11T20:22:14.710' AS DateTime), NULL, NULL, N'9781451673319', N'Fahrenheit 451', N'Guy Montag is a fireman. His job is to destroy the most illegal of commodities, the printed book, along with the houses in which they are hidden. Montag never questions the destruction and ruin his actions produce, returning each day to his bland life and wife, Mildred, who spends all day with her television “family.” But when he meets an eccentric young neighbor, Clarisse, who introduces him to a past where people didn’t live in fear and to a present where one sees the world through the ideas in books instead of the mindless chatter of television, Montag begins to question everything he has ever known.', 8, 4, 2.0000, 6.9400)
INSERT [dbo].[Books] ([BookId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [ISBN], [Title], [Description], [Author], [StockCount], [Cost], [Price]) VALUES (12, 0, 1, CAST(N'2020-11-11T20:24:11.180' AS DateTime), NULL, NULL, N'0307588378', N' Gone Girl', N'On a warm summer morning in North Carthage, Missouri, it is Nick and Amy Dunne’s fifth wedding anniversary. Presents are being wrapped and reservations are being made when Nick’s clever and beautiful wife disappears. Husband-of-the-Year Nick isn’t doing himself any favors with cringe-worthy daydreams about the slope and shape of his wife’s head, but passages from Amy''s diary reveal the alpha-girl perfectionist could have put anyone dangerously on edge. Under mounting pressure from the police and the media—as well as Amy’s fiercely doting parents—the town golden boy parades an endless series of lies, deceits, and inappropriate behavior. Nick is oddly evasive, and he’s definitely bitter—but is he really a killer? ', 9, 20, 8.0000, 15.3000)
INSERT [dbo].[Books] ([BookId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [ISBN], [Title], [Description], [Author], [StockCount], [Cost], [Price]) VALUES (13, 0, 1, CAST(N'2020-11-11T20:25:23.080' AS DateTime), NULL, NULL, N'0060775858', N'Goodnight Moon', N'In a great green room, tucked away in bed, is a little bunny. "Goodnight room, goodnight moon." And to all the familiar things in the softly lit room—to the picture of the three little bears sitting on chairs, to the clocks and his socks, to the mittens and the kittens, to everything one by one—the little bunny says goodnight.', 10, 1, 2.0000, 5.0000)
SET IDENTITY_INSERT [dbo].[Books] OFF
GO
SET IDENTITY_INSERT [dbo].[Customers] ON 

INSERT [dbo].[Customers] ([CustomerId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [FirstName], [LastName], [AddressId], [PhoneNumber], [FaxNumber], [MobileNumber]) VALUES (1, 0, 1, CAST(N'2020-11-11T20:28:15.870' AS DateTime), NULL, NULL, N'James', N'May', 1, N'4445551110', N'5555551110', N'6665551110')
INSERT [dbo].[Customers] ([CustomerId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [FirstName], [LastName], [AddressId], [PhoneNumber], [FaxNumber], [MobileNumber]) VALUES (2, 0, 1, CAST(N'2020-11-11T20:29:19.883' AS DateTime), NULL, NULL, N'Richard', N'Hammond', 2, N'4445551111', N'5555551111', N'6665551111')
INSERT [dbo].[Customers] ([CustomerId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [FirstName], [LastName], [AddressId], [PhoneNumber], [FaxNumber], [MobileNumber]) VALUES (3, 0, 1, CAST(N'2020-11-11T20:29:38.313' AS DateTime), NULL, NULL, N'Jeremy', N'Clarkson', 3, N'4445551112', N'5555551112', N'6665551112')
INSERT [dbo].[Customers] ([CustomerId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [FirstName], [LastName], [AddressId], [PhoneNumber], [FaxNumber], [MobileNumber]) VALUES (4, 0, 1, CAST(N'2020-11-11T20:29:52.830' AS DateTime), NULL, NULL, N'Jacob', N'DeGrom', 4, N'4445551113', N'5555551113', N'6665551113')
INSERT [dbo].[Customers] ([CustomerId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [FirstName], [LastName], [AddressId], [PhoneNumber], [FaxNumber], [MobileNumber]) VALUES (5, 0, 1, CAST(N'2020-11-11T20:30:07.703' AS DateTime), NULL, NULL, N'Pete', N'Alonso', 5, N'4445551114', N'5555551114', N'6665551114')
INSERT [dbo].[Customers] ([CustomerId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [FirstName], [LastName], [AddressId], [PhoneNumber], [FaxNumber], [MobileNumber]) VALUES (6, 0, 1, CAST(N'2020-11-11T20:30:50.503' AS DateTime), NULL, NULL, N'Jeff', N'McNeil', 6, N'4445551115', N'5555551115', N'6665551115')
INSERT [dbo].[Customers] ([CustomerId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [FirstName], [LastName], [AddressId], [PhoneNumber], [FaxNumber], [MobileNumber]) VALUES (7, 0, 1, CAST(N'2020-11-11T20:31:17.557' AS DateTime), NULL, NULL, N'Dominic', N'Smith', 7, N'4445551116', N'5555551116', N'6665551116')
INSERT [dbo].[Customers] ([CustomerId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [FirstName], [LastName], [AddressId], [PhoneNumber], [FaxNumber], [MobileNumber]) VALUES (8, 0, 1, CAST(N'2020-11-11T20:31:48.833' AS DateTime), NULL, NULL, N'Robinson', N'Cano', 8, N'4445551117', N'5555551117', N'6665551117')
INSERT [dbo].[Customers] ([CustomerId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [FirstName], [LastName], [AddressId], [PhoneNumber], [FaxNumber], [MobileNumber]) VALUES (9, 0, 1, CAST(N'2020-11-11T20:32:07.407' AS DateTime), NULL, NULL, N'Marcus', N'Stroman', 9, N'4445551118', N'5555551118', N'6665551118')
INSERT [dbo].[Customers] ([CustomerId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [FirstName], [LastName], [AddressId], [PhoneNumber], [FaxNumber], [MobileNumber]) VALUES (10, 0, 1, CAST(N'2020-11-11T20:32:22.117' AS DateTime), NULL, NULL, N'Noah', N'Syndergaard', 10, N'4445551119', N'5555551119', N'6665551119')
SET IDENTITY_INSERT [dbo].[Customers] OFF
GO
SET IDENTITY_INSERT [dbo].[OrderLines] ON 

INSERT [dbo].[OrderLines] ([OrderLineId], [Order], [Book], [Quantity], [Price]) VALUES (1, 1, 1, 2, 6.7500)
INSERT [dbo].[OrderLines] ([OrderLineId], [Order], [Book], [Quantity], [Price]) VALUES (2, 1, 3, 1, 10.6900)
INSERT [dbo].[OrderLines] ([OrderLineId], [Order], [Book], [Quantity], [Price]) VALUES (3, 2, 2, 1, 7.5500)
INSERT [dbo].[OrderLines] ([OrderLineId], [Order], [Book], [Quantity], [Price]) VALUES (4, 3, 3, 2, 10.6900)
INSERT [dbo].[OrderLines] ([OrderLineId], [Order], [Book], [Quantity], [Price]) VALUES (5, 3, 4, 1, 7.9200)
INSERT [dbo].[OrderLines] ([OrderLineId], [Order], [Book], [Quantity], [Price]) VALUES (7, 4, 12, 1, 15.3000)
INSERT [dbo].[OrderLines] ([OrderLineId], [Order], [Book], [Quantity], [Price]) VALUES (8, 5, 13, 1, 5.0000)
INSERT [dbo].[OrderLines] ([OrderLineId], [Order], [Book], [Quantity], [Price]) VALUES (9, 6, 5, 1, 5.3500)
INSERT [dbo].[OrderLines] ([OrderLineId], [Order], [Book], [Quantity], [Price]) VALUES (10, 6, 6, 1, 5.9500)
INSERT [dbo].[OrderLines] ([OrderLineId], [Order], [Book], [Quantity], [Price]) VALUES (11, 7, 8, 1, 9.4900)
INSERT [dbo].[OrderLines] ([OrderLineId], [Order], [Book], [Quantity], [Price]) VALUES (12, 8, 11, 1, 6.9400)
INSERT [dbo].[OrderLines] ([OrderLineId], [Order], [Book], [Quantity], [Price]) VALUES (13, 9, 6, 1, 5.9500)
INSERT [dbo].[OrderLines] ([OrderLineId], [Order], [Book], [Quantity], [Price]) VALUES (14, 10, 4, 3, 7.9200)
INSERT [dbo].[OrderLines] ([OrderLineId], [Order], [Book], [Quantity], [Price]) VALUES (15, 10, 1, 1, 6.7500)
INSERT [dbo].[OrderLines] ([OrderLineId], [Order], [Book], [Quantity], [Price]) VALUES (16, 11, 13, 1, 5.0000)
INSERT [dbo].[OrderLines] ([OrderLineId], [Order], [Book], [Quantity], [Price]) VALUES (17, 12, 11, 1, 6.9400)
INSERT [dbo].[OrderLines] ([OrderLineId], [Order], [Book], [Quantity], [Price]) VALUES (18, 13, 12, 1, 15.3000)
SET IDENTITY_INSERT [dbo].[OrderLines] OFF
GO
SET IDENTITY_INSERT [dbo].[Orders] ON 

INSERT [dbo].[Orders] ([OrderId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [Customer], [OrderStatus]) VALUES (1, 0, 1, CAST(N'2020-11-11T20:33:13.660' AS DateTime), NULL, NULL, 1, 1)
INSERT [dbo].[Orders] ([OrderId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [Customer], [OrderStatus]) VALUES (2, 0, 1, CAST(N'2020-11-11T20:34:00.420' AS DateTime), NULL, NULL, 1, 2)
INSERT [dbo].[Orders] ([OrderId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [Customer], [OrderStatus]) VALUES (3, 0, 1, CAST(N'2020-11-11T20:34:04.323' AS DateTime), NULL, NULL, 2, 2)
INSERT [dbo].[Orders] ([OrderId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [Customer], [OrderStatus]) VALUES (4, 0, 1, CAST(N'2020-11-11T20:34:07.000' AS DateTime), NULL, NULL, 2, 2)
INSERT [dbo].[Orders] ([OrderId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [Customer], [OrderStatus]) VALUES (5, 0, 1, CAST(N'2020-11-11T20:34:12.147' AS DateTime), NULL, NULL, 3, 2)
INSERT [dbo].[Orders] ([OrderId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [Customer], [OrderStatus]) VALUES (6, 0, 1, CAST(N'2020-11-11T20:34:18.613' AS DateTime), NULL, NULL, 3, 2)
INSERT [dbo].[Orders] ([OrderId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [Customer], [OrderStatus]) VALUES (7, 0, 1, CAST(N'2020-11-11T20:34:26.147' AS DateTime), NULL, NULL, 4, 1)
INSERT [dbo].[Orders] ([OrderId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [Customer], [OrderStatus]) VALUES (8, 0, 1, CAST(N'2020-11-11T20:34:32.223' AS DateTime), NULL, NULL, 5, 2)
INSERT [dbo].[Orders] ([OrderId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [Customer], [OrderStatus]) VALUES (9, 0, 1, CAST(N'2020-11-11T20:34:37.460' AS DateTime), NULL, NULL, 6, 2)
INSERT [dbo].[Orders] ([OrderId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [Customer], [OrderStatus]) VALUES (10, 0, 1, CAST(N'2020-11-11T20:34:43.503' AS DateTime), NULL, NULL, 7, 2)
INSERT [dbo].[Orders] ([OrderId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [Customer], [OrderStatus]) VALUES (11, 0, 1, CAST(N'2020-11-11T20:34:49.077' AS DateTime), NULL, NULL, 7, 2)
INSERT [dbo].[Orders] ([OrderId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [Customer], [OrderStatus]) VALUES (12, 0, 1, CAST(N'2020-11-11T20:34:55.280' AS DateTime), NULL, NULL, 8, 1)
INSERT [dbo].[Orders] ([OrderId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [Customer], [OrderStatus]) VALUES (13, 0, 1, CAST(N'2020-11-11T20:35:03.887' AS DateTime), NULL, NULL, 9, 2)
SET IDENTITY_INSERT [dbo].[Orders] OFF
GO
SET IDENTITY_INSERT [dbo].[OrderStatus] ON 

INSERT [dbo].[OrderStatus] ([OrderStatusId], [Name]) VALUES (1, N'Open')
INSERT [dbo].[OrderStatus] ([OrderStatusId], [Name]) VALUES (2, N'Fulfilled')
SET IDENTITY_INSERT [dbo].[OrderStatus] OFF
GO
SET IDENTITY_INSERT [dbo].[Users] ON 

INSERT [dbo].[Users] ([UserId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [FirstName], [LastName], [Email]) VALUES (1, 0, 1, CAST(N'2020-11-11T19:55:09.147' AS DateTime), NULL, NULL, N'Robert', N'Morrissey', N'rmorrissey@fakeEmail.com')
INSERT [dbo].[Users] ([UserId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [FirstName], [LastName], [Email]) VALUES (2, 0, 1, CAST(N'2020-11-11T19:55:09.147' AS DateTime), NULL, NULL, N'John', N'Doe', N'jdoe@fakeEmail.com')
INSERT [dbo].[Users] ([UserId], [Archived], [CreatedBy], [DateCreated], [ModifiedBy], [DateModified], [FirstName], [LastName], [Email]) VALUES (3, 0, 1, CAST(N'2020-11-11T19:55:09.147' AS DateTime), NULL, NULL, N'Jacob', N'Degrom', N'jdegrom@fakeEmail.com')
SET IDENTITY_INSERT [dbo].[Users] OFF
GO
ALTER TABLE [dbo].[Address] ADD  DEFAULT ((0)) FOR [Archived]
GO
ALTER TABLE [dbo].[Address] ADD  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [dbo].[Authors] ADD  DEFAULT ((0)) FOR [Archived]
GO
ALTER TABLE [dbo].[Authors] ADD  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [dbo].[Books] ADD  DEFAULT ((0)) FOR [Archived]
GO
ALTER TABLE [dbo].[Books] ADD  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [dbo].[Customers] ADD  DEFAULT ((0)) FOR [Archived]
GO
ALTER TABLE [dbo].[Customers] ADD  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [dbo].[Orders] ADD  DEFAULT ((0)) FOR [Archived]
GO
ALTER TABLE [dbo].[Orders] ADD  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [dbo].[Users] ADD  DEFAULT ((0)) FOR [Archived]
GO
ALTER TABLE [dbo].[Users] ADD  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [dbo].[Authors]  WITH CHECK ADD FOREIGN KEY([AddressId])
REFERENCES [dbo].[Address] ([AddressId])
GO
ALTER TABLE [dbo].[Books]  WITH CHECK ADD FOREIGN KEY([Author])
REFERENCES [dbo].[Authors] ([AuthorId])
GO
ALTER TABLE [dbo].[Customers]  WITH CHECK ADD FOREIGN KEY([AddressId])
REFERENCES [dbo].[Address] ([AddressId])
GO
ALTER TABLE [dbo].[OrderLines]  WITH CHECK ADD FOREIGN KEY([Order])
REFERENCES [dbo].[Orders] ([OrderId])
GO
ALTER TABLE [dbo].[OrderLines]  WITH CHECK ADD FOREIGN KEY([Book])
REFERENCES [dbo].[Books] ([BookId])
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD FOREIGN KEY([Customer])
REFERENCES [dbo].[Customers] ([CustomerId])
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD FOREIGN KEY([OrderStatus])
REFERENCES [dbo].[OrderStatus] ([OrderStatusId])
GO
