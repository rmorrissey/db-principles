USE RMorrissey

IF OBJECT_ID('dbo.BookCategories', 'U') IS NOT NULL
	DROP TABLE dbo.BookCategories
GO

CREATE TABLE dbo.BookCategories (
	BookCategoriesId INT IDENTITY(1, 1) PRIMARY KEY,
	BookId INT NOT NULL FOREIGN KEY REFERENCES Books,
	CategoryId INT NOT NULL FOREIGN KEY REFERENCES Categories
	);
GO


