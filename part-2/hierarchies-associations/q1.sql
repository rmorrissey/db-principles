USE RMorrissey

BEGIN
	DECLARE @LastMonth DATE = DATEADD(mm, - 1, GETDATE())

	SELECT *
	FROM Books b
	WHERE NOT EXISTS (
			SELECT BookId
			FROM OrderLines ol
			JOIN Orders o ON ol.[Order] = o.OrderId
			WHERE b.BookId = ol.Book
				AND o.DateCreated > @LastMonth
			)
END
GO

BEGIN
	DECLARE @LastMonth DATE = GETDATE()

	SELECT *
	FROM Books b
	WHERE NOT EXISTS (
			SELECT BookId
			FROM OrderLines ol
			JOIN Orders o ON ol.[Order] = o.OrderId
			WHERE b.BookId = ol.Book
				AND DATEDIFF(mm, o.DateCreated, @LastMonth) = 0
			)
END
GO
