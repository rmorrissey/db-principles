USE RMorrissey -- Create a new table called 'Categories' in schema 'dbo'
	-- Drop the table if it already exists

IF OBJECT_ID('dbo.Categories', 'U') IS NOT NULL
	DROP TABLE dbo.Categories
GO

-- Create the table in the specified schema
CREATE TABLE dbo.Categories (
	CategoryId INT IDENTITY(1, 1) PRIMARY KEY,
	-- primary key column
	Archived BIT NOT NULL DEFAULT 0,
	CreatedBy INT,
	DateCreated DATETIME DEFAULT GETDATE(),
	ModifiedBy INT,
	DateModified DATETIME,
	ParentId INT NOT NULL,
	Name [NVARCHAR](50) NOT NULL
	);
GO


