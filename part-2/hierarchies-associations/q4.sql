USE RMorrissey

IF EXISTS (
		SELECT *
		FROM INFORMATION_SCHEMA.ROUTINES
		WHERE SPECIFIC_SCHEMA = N'dbo'
			AND SPECIFIC_NAME = N'SetBookCatergory'
		)
	DROP PROCEDURE dbo.SetBookCatergory
GO

CREATE PROCEDURE dbo.SetBookCatergory @BookId INT,
	@CategoryId INT
AS
INSERT INTO BookCategories
VALUES (
	@BookId,
	@CategoryId
	)
GO

EXECUTE dbo.SetBookCatergory 4,
	3
GO


