USE RMorrissey
GO

IF EXISTS (
		SELECT *
		FROM INFORMATION_SCHEMA.ROUTINES
		WHERE SPECIFIC_SCHEMA = N'dbo'
			AND SPECIFIC_NAME = N'AllBookTags'
		)
	DROP FUNCTION dbo.AllBookTags
GO

CREATE FUNCTION AllBookTags (@BookId INT)
RETURNS [VARCHAR] (MAX)
AS
BEGIN
	DECLARE @listStr VARCHAR(MAX);

	WITH cte
	AS (
		SELECT bc.CategoryId,
			c.ParentId,
			c.Name
		FROM BookCategories bc
		JOIN Categories c ON bc.CategoryId = c.CategoryId
		WHERE bc.BookId = @BookId
		
		UNION ALL
		
		SELECT p.CategoryId,
			p.ParentId,
			p.Name
		FROM Categories p
		JOIN cte ON cte.ParentId = p.CategoryId
		)
	SELECT @listStr = COALESCE(@listStr + ', ', '') + Name
	FROM (
		SELECT DISTINCT *
		FROM cte
		) dt

	RETURN @listStr
END
GO

SELECT dbo.AllBookTags(5)
