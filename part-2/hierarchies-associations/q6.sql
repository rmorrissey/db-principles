USE RMorrissey

SELECT b.BookId,
	b.Title,
	dbo.AllBookTags(b.BookId) AS Tags
FROM Books b
WHERE b.BookId = 5
