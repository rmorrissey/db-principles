USE RMorrissey

IF EXISTS (
		SELECT *
		FROM INFORMATION_SCHEMA.ROUTINES
		WHERE SPECIFIC_SCHEMA = N'dbo'
			AND SPECIFIC_NAME = N'GetAuthorSales'
		)
	DROP PROCEDURE dbo.GetAuthorSales
GO

CREATE PROCEDURE dbo.GetAuthorSales @AuthorId INT
AS
DECLARE @strList [VARCHAR] (MAX);

WITH CTE
AS (
	SELECT b.BookId,
		b.Title,
		dt.[State],
		SUM(dt.SoldInState) AS CopiesSoldInState,
		SUM(dt.StateTotalPrice) AS TotalPriceSold
	FROM Books b
	OUTER APPLY (
		SELECT dt2.[State],
			SUM(ol.Quantity) AS SoldInState,
			ISNULL(SUM(ol.Quantity * ol.Price), 0) AS StateTotalPrice
		FROM OrderLines ol
		OUTER APPLY (
			SELECT a.[State]
			FROM Orders o
			JOIN Customers c ON o.Customer = c.CustomerId
			JOIN Address a ON c.AddressId = a.AddressId
			WHERE o.OrderId = ol.[Order]
			) dt2
		WHERE b.BookId = ol.Book
		GROUP BY dt2.[State]
		) dt
	WHERE b.Author = @AuthorId
	GROUP BY b.Title,
		b.BookId,
		dt.[State]
	)
SELECT DISTINCT c.Title,
	ISNULL(SUM(copiesSoldInState), 0) AS TotalCopiesSold,
	ISNULL(SUM(TotalPriceSold), 0) AS TotalPriceSold,
	REVERSE(STUFF(REVERSE(LTRIM(RTRIM(BestSellingStates))), 1, CASE 
				WHEN SUBSTRING((REVERSE(LTRIM(RTRIM(BestSellingStates)))), 1, 1) = ','
					THEN 1
				ELSE 0
				END, '')) AS BestSellingStates
FROM CTE c
CROSS APPLY (
	SELECT c1.[State] + ', '
	FROM CTE c1
	WHERE c1.CopiesSoldInState = (
			SELECT MAX(CopiesSoldInState)
			FROM CTE c2
			WHERE c2.BookId = c1.BookId
			)
		AND c1.BookId = c.BookId
	ORDER BY c1.[State]
	FOR XML PATH('')
	) AS dt(BestSellingStates)
GROUP BY Title,
	BestSellingStates
GO

EXECUTE dbo.GetAuthorSales 4
GO


