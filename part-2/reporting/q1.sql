USE RMorrissey

IF EXISTS (
		SELECT *
		FROM INFORMATION_SCHEMA.ROUTINES
		WHERE SPECIFIC_SCHEMA = N'dbo'
			AND SPECIFIC_NAME = N'SalesPerDay'
		)
	DROP PROCEDURE dbo.SalesPerDay
GO

CREATE PROCEDURE dbo.SalesPerDay @StartDate DATE,
	@EndDate DATE,
	@CustState [VARCHAR] (2) = NULL,
	@BookCategory [VARCHAR] (50) = NULL
AS
WITH Dates
AS (
	SELECT @StartDate AS ReportDate
	
	UNION ALL
	
	SELECT DATEADD(d, 1, ReportDate) AS ReportDate
	FROM Dates
	WHERE DATEADD(d, 1, ReportDate) < @EndDate
	)
SELECT d.ReportDate,
	COUNT(o.OrderId) AS OrderCount,
	SUM(CASE 
			WHEN o.OrderStatus = 1
				THEN 1
			ELSE 0
			END) AS OpenOrderCount,
	COUNT(o.TotalPrice) AS BooksSold,
	ISNULL(SUM(o.TotalPrice), 0)
FROM Dates d
LEFT OUTER JOIN (
	SELECT o.OrderId,
		o.DateCreated,
		o.OrderStatus,
		ol.TotalPrice
	FROM Orders o
	JOIN (
		SELECT ol.[Order],
			SUM(ol.Price * ol.Quantity) AS TotalPrice
		FROM OrderLines ol
		LEFT JOIN BookCategories bc ON ol.Book = bc.BookId
		LEFT JOIN Categories c ON bc.CategoryId = c.CategoryId
		WHERE (
				@BookCategory IS NULL
				OR (c.Name = @BookCategory)
				)
		GROUP BY ol.[Order]
		) AS ol ON o.OrderId = ol.[Order]
	JOIN (
		SELECT c.CustomerId,
			a.[State] AS CustomerState
		FROM Customers c
		JOIN Address a ON c.AddressId = a.AddressId
		WHERE (
				@CustState IS NULL
				OR (a.[State] = @CustState)
				)
		) AS c ON o.Customer = c.CustomerId
	) AS o ON d.ReportDate = CAST(o.DateCreated AS DATE)
GROUP BY d.ReportDate
GO

DECLARE @today DATE = GETDATE(),
	@lastWeek DATE = DATEADD(d, - 7, GETDATE())

EXECUTE dbo.SalesPerDay @lastWeek,
	@today,
	WA
GO


