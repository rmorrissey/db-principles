USE RMorrissey

IF EXISTS (
		SELECT *
		FROM INFORMATION_SCHEMA.ROUTINES
		WHERE SPECIFIC_SCHEMA = N'dbo'
			AND SPECIFIC_NAME = N'getOrderLines'
		)
	DROP PROCEDURE dbo.getOrderLines
GO

CREATE PROCEDURE dbo.getOrderLines @OrderId INT
AS
BEGIN
	SELECT ol.[Order],
		b.Title,
		ol.Quantity,
		ol.Price
	FROM dbo.OrderLines ol
	JOIN Books b ON ol.Book = b.BookId
	WHERE ol.[Order] = @OrderId
END
GO


