USE RMorrissey

IF EXISTS (
		SELECT *
		FROM INFORMATION_SCHEMA.ROUTINES
		WHERE SPECIFIC_SCHEMA = N'dbo'
			AND SPECIFIC_NAME = N'GetOrderInfo'
		)
	DROP PROCEDURE dbo.GetOrderInfo
GO

CREATE PROCEDURE dbo.GetOrderInfo @OrderId INT
AS
BEGIN
	SELECT o.orderId,
		o.DateCreated,
		c.FirstName,
		c.LastName,
		a.StreetLine1,
		a.StreetLine2,
		a.City,
		a.[State],
		a.ZipCode,
		SUM(l.Quantity * l.Price) AS Total
	FROM dbo.Orders o
	JOIN Customers c ON o.Customer = c.CustomerId
	JOIN OrderLines l ON o.OrderId = l.[Order]
	JOIN Address a ON c.AddressId = a.AddressId
	WHERE o.OrderId = @OrderId
	GROUP BY o.orderId,
		o.DateCreated,
		c.FirstName,
		c.LastName,
		a.StreetLine1,
		a.StreetLine2,
		a.City,
		a.[State],
		a.ZipCode
END
GO


