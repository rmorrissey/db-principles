USE RMorrissey

IF EXISTS (
		SELECT *
		FROM sys.VIEWS
		JOIN sys.schemas ON sys.VIEWS.schema_id = sys.schemas.schema_id
		WHERE sys.schemas.name = N'dbo'
			AND sys.VIEWS.name = N'OpenOrders'
		)
	DROP VIEW dbo.OpenOrders
GO

CREATE VIEW dbo.OpenOrders
AS
SELECT o.OrderId,
	o.DateCreated,
	c.LastName,
	c.FirstName,
	SUM(ol.Price * ol.Quantity) AS TotalPrice,
	MAX(os.Name) AS OrderStatus
FROM dbo.Orders o
JOIN OrderStatus os ON o.OrderStatus = os.OrderStatusId
JOIN Customers c ON o.Customer = c.CustomerId
JOIN OrderLines ol ON o.OrderId = ol.[Order]
WHERE os.Name = 'Open'
GROUP BY o.OrderId,
	o.DateCreated,
	c.LastName,
	c.FirstName
GO
