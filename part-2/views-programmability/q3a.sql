USE RMorrissey

IF EXISTS (
		SELECT *
		FROM INFORMATION_SCHEMA.ROUTINES
		WHERE SPECIFIC_SCHEMA = N'dbo'
			AND SPECIFIC_NAME = N'CreateCustomerOrder'
		)
	DROP PROCEDURE dbo.CreateCustomerOrder
GO

CREATE PROCEDURE dbo.CreateCustomerOrder @CustomerId INT = REQUIRED
AS
BEGIN
	SET NOCOUNT ON

	INSERT INTO Orders (
		[Customer],
		[OrderStatus]
		)
	VALUES (
		@CustomerId,
		1
		)

	SELECT SCOPE_IDENTITY()
END
GO
