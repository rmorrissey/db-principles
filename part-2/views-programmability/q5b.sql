USE RMorrissey
GO

IF EXISTS (
		SELECT *
		FROM INFORMATION_SCHEMA.ROUTINES
		WHERE SPECIFIC_SCHEMA = N'dbo'
			AND SPECIFIC_NAME = N'CustomerOpenOrderTotalPrice'
		)
	DROP FUNCTION dbo.CustomerOpenOrderTotalPrice
GO

CREATE FUNCTION CustomerOpenOrderTotalPrice (@CustomerId INT)
RETURNS TABLE
AS
RETURN (
		SELECT SUM(ol.Price * ol.Quantity) AS OpenOrdersTotalPrice
		FROM Orders o
		JOIN OrderStatus os ON o.OrderStatus = os.OrderStatusId
		JOIN OrderLines ol ON o.OrderId = ol.[Order]
		WHERE o.Customer = @CustomerId
			AND os.Name = 'Open'
		)
GO


