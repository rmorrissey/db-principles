USE RMorrissey
GO

IF EXISTS (
		SELECT *
		FROM INFORMATION_SCHEMA.ROUTINES
		WHERE SPECIFIC_SCHEMA = N'dbo'
			AND SPECIFIC_NAME = N'CustomerOpenOrderCount'
		)
	DROP FUNCTION dbo.CustomerOpenOrderCount
GO

CREATE FUNCTION CustomerOpenOrderCount (@CustomerId INT)
RETURNS TABLE
AS
RETURN (
		SELECT COUNT(o.OrderId) AS OpenOrders
		FROM Orders o
		JOIN OrderStatus os ON o.OrderStatus = os.OrderStatusId
		WHERE o.Customer = @CustomerId
			AND os.Name = 'Open'
		)
GO


