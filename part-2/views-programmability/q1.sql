USE RMorrissey -- Create a new view called 'ActiveBooks' in schema 'dbo'
-- Drop the view if it already exists
IF EXISTS (
    SELECT
        *
    FROM
        sys.views
        JOIN sys.schemas ON sys.views.schema_id = sys.schemas.schema_id
    WHERE
        sys.schemas.name = N'dbo'
        AND sys.views.name = N'ActiveBooks'
) DROP VIEW dbo.ActiveBooks
GO
    -- Create the view in the specified schema
    CREATE VIEW dbo.ActiveBooks AS -- body of the view
SELECT
    *
FROM
    dbo.Books b
WHERE
    b.Archived = 0
GO