USE RMorrissey

BEGIN
	DECLARE @id INT = 1

	SELECT c.CustomerId,
		c.FirstName,
		c.LastName,
		oo.OpenOrders,
		tp.OpenOrdersTotalPrice
	FROM Customers c,
		dbo.CustomerOpenOrderCount(@id) oo,
		dbo.CustomerOpenOrderTotalPrice(@id) tp
	WHERE c.CustomerId = @id
END
