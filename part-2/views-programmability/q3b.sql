USE RMorrissey

IF EXISTS (
		SELECT *
		FROM INFORMATION_SCHEMA.ROUTINES
		WHERE SPECIFIC_SCHEMA = N'dbo'
			AND SPECIFIC_NAME = N'CreateNewOrderLine'
		)
	DROP PROCEDURE dbo.CreateNewOrderLine
GO

CREATE PROCEDURE dbo.CreateNewOrderLine @OrderId INT,
	@BookId INT,
	@Quantity INT = 1
AS
BEGIN
	DECLARE @Price MONEY

	UPDATE Books
	SET StockCount = StockCount - @Quantity,
		@Price = Price
	WHERE BookId = @BookId

	INSERT INTO OrderLines (
		[Order],
		[Book],
		[Quantity],
		[Price]
		)
	VALUES (
		@OrderId,
		@BookId,
		@Quantity,
		@Price
		)
END
GO
